<nav class="side-nav">
    <a href="{{route('template.index')}}" class="intro-x flex items-center pl-5 pt-4">
        <img alt="Mao Leng" class="w-6" src="{{asset('app/images/logo.png')}}" style="width:40px">
        <span class="hidden xl:block text-white text-lg ml-3">
            MaoLeng
        </span>
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul>
        <li>
            <a href="{{route('template.index')}}" class="side-menu side-menu--active">
                <div class="side-menu__icon">
                    <i data-lucide="home"></i>
                </div>
                <div class="side-menu__title">
                    Mẫu tin nhắn
                </div>
            </a>
        </li>



    </ul>
</nav>
