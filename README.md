<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<h1 align="center">Email Daily</h1>

## Phát triển
- Bùi Hữu Lộc

## Chức năng chính
- Đăng nhập qua app (Google, GitHub, GitLab, Facebook, Linkedin, Twitter)
- Lên lịch gửi mail
- Tự động gửi mail sau mỗi khoảng thời gian

## Một số hình ảnh
<table>
    <tr>
        <img src="https://user-images.githubusercontent.com/91431461/186601858-a49766e7-2b43-457c-9470-40a1410e9a32.png">
    </tr>
    <tr>
        <td width="70%">
            <img src="https://user-images.githubusercontent.com/91431461/186604216-e1e3aedf-2b4f-41d2-9c10-7b43b30498c5.png">
        </td>
        <td width="30%">
            <img height="90%" src="https://user-images.githubusercontent.com/91431461/186604090-0152fc20-49a7-4fdb-a557-52f073503653.png">
        </td>
    </tr>
</table>
